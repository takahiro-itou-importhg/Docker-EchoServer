
#include    <memory.h>
#include    <netinet/in.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <sys/socket.h>
#include    <unistd.h>

enum {
    QUEUE_LIMIT = 5
};

const char  STARTUP_MESSAGE[]   = "WELCOME, THIS IS ECHO SERVER.\n";

int  main(int argc,  char * argv[])
{
    int     sockServer;
    int     sockClient;
    struct  sockaddr_in     sockaddrServer;
    struct  sockaddr_in     sockaddrClient;

    unsigned short  serverPort;
    socklen_t       lenClient;

    if ( argc != 2 ) {
        fprintf(stderr,  "Not Enough Args\n");
        return ( EXIT_FAILURE );
    }

    serverPort  = static_cast<unsigned short>(atoi(argv[1]));
    if ( serverPort == 0 ) {
        fprintf(stderr,  "Invalid Port Number\n");
        return ( EXIT_FAILURE );
    }

    sockServer  = ::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if ( sockServer < 0 ) {
        ::perror("Failed socket().");
        return ( EXIT_FAILURE );
    }

    ::memset(&sockaddrServer, 0, sizeof(sockaddrServer));
    sockaddrServer.sin_family       = AF_INET;
    sockaddrServer.sin_addr.s_addr  = htonl(INADDR_ANY);
    sockaddrServer.sin_port         = htons(serverPort);

    if ( ::bind(    sockServer,
                    (struct sockaddr *)(&sockaddrServer),
                    sizeof(sockaddrServer)) < 0 )
    {
        ::perror("Failed bind().");
        return ( EXIT_FAILURE );
    }

    if ( ::listen(sockServer, QUEUE_LIMIT) < 0 ) {
        ::perror("Failed listen().");
        return ( EXIT_FAILURE );
    }

    for (;;) {
        lenClient   = sizeof(sockaddrClient);
        sockClient  = ::accept(
                            sockServer,
                            (struct sockaddr *)(&sockaddrClient),
                            &lenClient);
        if ( sockClient < 0 ) {
            ::perror("Failed accept().");
            continue;
        }

        ::write(sockClient, STARTUP_MESSAGE, strlen(STARTUP_MESSAGE));
        ::close(sockClient);
    }

    ::close(sockServer);
    return ( 0 );
}
